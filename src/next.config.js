// next.config.js
const withCSS = require('@zeit/next-css')
const withSCSS = require('@zeit/next-sass');
const withReactSvg = require('next-react-svg');
const path = require('path');

module.exports = withCSS(withSCSS(withReactSvg({
    include: path.resolve(__dirname, 'assets/icons'),
    webpack(config, options) {
        return config;
    }
})));
