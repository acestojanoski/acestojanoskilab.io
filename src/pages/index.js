import "bootstrap/dist/css/bootstrap.css";
import "../styles/global.scss";
import { Component } from "react";
import { Container, Row, Col } from 'react-bootstrap';
import Header from "../components/Header";
import About from '../components/About';

class IndexPage extends Component {
    render() {
        return (
            <div>
                <Header />
                <Container>
                    <Row>
                        <Col>
                            <About />
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default IndexPage;
