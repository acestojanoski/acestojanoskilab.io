import "bootstrap/dist/css/bootstrap.css";
import "../styles/global.scss";
import { Component } from "react";
import { Container, Row, Col } from 'react-bootstrap';
import Header from "../components/Header";
import Contact from "../components/Contact";

class IndexPage extends Component {
    render() {
        return (
            <div>
                <Header />
                <Container>
                    <Row>
                        <Contact />
                    </Row>
                </Container>
            </div>
        );
    }
}

export default IndexPage;
