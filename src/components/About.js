import { Component } from "react";
import { Container, Row, Col, Jumbotron } from "react-bootstrap";
import Linkedin from '../assets/icons/linkedin.svg';
import Gitlab from '../assets/icons/gitlab.svg';
import Github from '../assets/icons/github.svg';
import Email from '../assets/icons/email.svg';

class About extends Component {
    render() {
        return (
            <Container className="page-section">
                <Row>
                    <Col>
                        <Jumbotron fluid>
                            <Container className="text-center">
                                <hr />
                                <h2>Aleksandar Stojanoski</h2>
                                <h5>Web developer</h5>
                                <hr />
                                <a
                                    href="mailto:aleksandar-stojanoski@hotmail.com"
                                    target="_blank"
                                >
                                    <Email className="link-icon-email" />
                                </a>
                                <a
                                    href="https://www.linkedin.com/in/aleksandar-stojanoski-44a888139"
                                    target="_blank"
                                >
                                    <Linkedin className="link-icon" />
                                </a>
                                <a
                                    href="https://gitlab.com/acestojanoski/"
                                    target="_blank"
                                >
                                    <Gitlab className="link-icon" />
                                </a>
                                <a
                                    href="https://github.com/acestojanoski"
                                    target="_blank"
                                >
                                    <Github className="link-icon" />
                                </a>
                            </Container>
                        </Jumbotron>
                    </Col>
                </Row>
            </Container >
        );
    }
}

export default About;
