import { Component } from "react";
import { Container, Row, Col, Jumbotron } from "react-bootstrap";

class Contact extends Component {
    render() {
        return (
            <Container className="page-section">
                <Row>
                    <Col>
                        <Jumbotron fluid>
                            <Container className="text-center">
                                <hr />
                                <h1>Contact</h1>
                                <hr />
                                <p>
                                    Email address:&nbsp;
                                    <a
                                        target="_blank"
                                        href="mailto:aleksandar-stojanoski@hotmail.com"
                                    >
                                        aleksandar-stojanoski@hotmail.com
                                    </a>
                                </p>
                                <p>
                                    LinkedIn:&nbsp;
                                    <a
                                        target="_blank"
                                        href="https://www.linkedin.com/in/aleksandar-stojanoski-44a888139"
                                    >
                                        Aleksandar Stojanoski
                                    </a>
                                </p>
                            </Container>
                        </Jumbotron>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Contact;
