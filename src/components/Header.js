import { Component } from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import router from 'next/router';
import Link from 'next/link';

class Header extends Component {
    render() {
        return (
            <Navbar sticky="top" collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Link href="/">
                    <a className="navbar-brand">
                        Aleksandar Stojanoski
                    </a>
                </Link>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                        <Link href="/">
                            <a className="nav-link">
                                Home
                            </a>
                        </Link>
                        <Link href="/contact">
                            <a className="nav-link">
                                Contact
                            </a>
                        </Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}

export default Header;
