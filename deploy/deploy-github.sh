#!/bin/bash

npm run export
sudo rm -r acestojanoski.github.io
git clone https://github.com/acestojanoski/acestojanoski.github.io.git
cd acestojanoski.github.io && sudo rm -r ./*
sudo cp -r ../src/out/* ./
git add .
git commit -m "deployment update"
git push origin master
cd .. && sudo rm -r acestojanoski.github.io
